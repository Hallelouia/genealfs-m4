/*
** close.c for  in /home/morand_c//genealfs-m4/genealfs
** 
** Made by raphael morand
** Login   <morand_c@epitech.net>
** 
** Started on  Fri Jun 28 09:08:22 2013 raphael morand
** Last update Fri Jun 28 09:23:02 2013 raphael morand
*/

#include "genealfs.h"

void closedirs(t_dlist *list)
{
    t_elem *elem;

    if (list == NULL)
        return;
    elem = list->first;
    while (elem != list->last)
    {
        closedir(elem->dir);
        elem = elem->next;
    }
}
