/*
** handle_option.c for  in /home/morand_c//genealfs-m4/genealfs/src
** 
** Made by raphael morand
** Login   <morand_c@epitech.net>
** 
** Started on  Thu Jun 27 09:35:14 2013 raphael morand
** Last update Fri Jun 28 09:19:10 2013 raphael morand
*/

#include "genealfs.h"

int dup_opt(char *file, char *path,
            t_genealfs *root, t_dlist *list)
{
    int fd;

    if ((fd = open(path, O_RDWR | O_CREAT | O_TRUNC)) == -1)
        return (-1);
    if (dup2(fd, 1) == -1)
        return (-1);
    list = genealfs(file, root, list);
    view(list);
    close(fd);
    closedirs(list);
    return (0);
}

int pipe_opt(char *file, char *prog,
             t_genealfs *root, t_dlist *list)
{
    int pfd[2];
    int pid;
    char **options;

    options = my_str_to_wordtab(prog, ' ');
    if ((pipe(pfd) == -1))
        return (-1);
    if ((pid = fork()) == -1)
        return (-1);
    if (pid == 0)
    {
        close(pfd[0]);
        if (dup2(pfd[1], 1) == -1)
            return (-1);
        list = genealfs(file, root, list);
        view(list);
        closedirs(list);
    }
    else
    {
        close(pfd[1]);
        if (dup2(pfd[0], 0) == -1)
            return (-1);
        execvp(*options, options);
    }
    free(options);
    return (0);
}

fct *opt_tab(fct *opt_fct)
{
    opt_fct[0] = &dup_opt;
    opt_fct[1] = &pipe_opt;
    opt_fct[2] = NULL;
    return (opt_fct);
}

char **opt_comp(char **comp)
{
    comp[0] = "-f";
    comp[1] = "-p";
    comp[2] = NULL;
    return (comp);
}

int check_option(char *opt)
{
    int i;
    char *opti_comp[3];
    int cmp;

    opt_comp(opti_comp);
    i = 0;
    while (i < 2)
    {
        cmp = strcmp(opt, opti_comp[i]);
        if (cmp == 0)
            return (i);
        i++;
    }
    return (-1);
}
