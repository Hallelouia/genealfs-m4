/*
** handle_termcaps2.c for  in /home/morand_c//genealfs-m4/genealfs
** 
** Made by raphael morand
** Login   <morand_c@epitech.net>
** 
** Started on  Thu Jun 27 12:34:01 2013 raphael morand
** Last update Fri Jun 28 08:32:58 2013 raphael morand
*/

#include "genealfs.h"

t_dlist *get_key(int c, t_dlist *d_list)
{
    int j;

    j = 0;
    if ((j = read(0, &c, sizeof(int))) <= 0)
        return (d_list);
    if (c == TOUCH_UP)
        d_list = xnext(d_list);
    else if (c == TOUCH_DOWN)
        d_list = xprev(d_list);
    else if (c == TOUCH_ENTER)
        d_list = xselect(d_list);
    else if (c == TOUCH_ESC)
        exit(EXIT_SUCCESS);
    return (d_list);
}

void save_termcaps(t_tsave *save)
{
    save->cl = tgetstr("cl", NULL);
    save->cm = tgetstr("cm", NULL);
    save->us = tgetstr("us", NULL);
    save->mr = tgetstr("mr", NULL);
    save->me = tgetstr("me", NULL);
}

void view_tc(t_dlist *list, t_tsave *s)
{
    t_elem *elem;
    int i;

    list->last->next = NULL;
    elem = list->first;
    printf("%s", s->cl);
    while (elem != NULL)
    {
        i = -1;
        if (elem->point == 1 && elem->select == 1)
            printsp(elem, i, s);
        else if (elem->point == 1 && elem->select == 0)
            printf("\e[1;34m%s%s%s\e[0m\n", s->us, elem->data, s->me);
        else if (elem->select == 1 && elem->point == 0)
            prints(elem, i, s);
        else
            printf("\e[1;34m%s\e[0m\n", elem->data);
        elem = elem->next;
    }
}

void my_select(t_dlist *d_list)
{
    int c;
    int i;
    t_tsave save;

    if (tgetent(NULL, "xterm") <= 0)
        exit(-1);
    save_termcaps(&save);
    i = 0;
    view_tc(d_list, &save);
    while (i == 0)
    {
        circularize(d_list);
        c = 0;
        if (d_list->size != 0)
        {
            d_list = get_key(c, d_list);
            view_tc(d_list, &save);
        }
        else
            i = 1;
    }
}
