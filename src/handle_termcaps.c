/*
** handle_termcaps.c for  in /home/morand_c//genealfs-m4/genealfs/src
** 
** Made by raphael morand
** Login   <morand_c@epitech.net>
** 
** Started on  Thu Jun 27 10:27:25 2013 raphael morand
** Last update Thu Jun 27 13:07:06 2013 raphael morand
*/

#include "genealfs.h"

void go_next(t_dlist *d_list)
{
    t_elem *elem;

    elem = d_list->first;
    while (elem->point != 1)
        elem = elem->next;
    if (elem->point == 1)
        elem->point = 0;
    elem = elem->next;
    elem->point = 1;
}

void go_prev(t_dlist *d_list)
{
    t_elem *elem;

    elem = d_list->last;
    while (elem->point != 1)
        elem = elem->prev;
    if (elem->point == 1)
        elem->point = 0;
    elem = elem->prev;
    elem->point = 1;
}

t_dlist *xnext(t_dlist *d_list)
{
    go_prev(d_list);
    (d_list->pos)--;
    if (d_list->pos <= 0)
        d_list->pos = d_list->size;
    return (d_list);
}

t_dlist *xprev(t_dlist *d_list)
{
    go_next(d_list);
    (d_list->pos)++;
    if (d_list->pos > d_list->size)
        d_list->pos = 1;
    return (d_list);
}

t_dlist *xselect(t_dlist *d_list)
{
    t_elem *elem;

    elem = d_list->first;
    while (elem->point != 1)
        elem = elem->next;
    if (elem->select == 0)
        elem->select = 1;
    else if (elem->select == 1)
        elem->select = 0;
    return (d_list);
}
