/*
** start.c for  in /home/morand_c//genealfs-m4/genealfs/src
** 
** Made by raphael morand
** Login   <morand_c@epitech.net>
** 
** Started on  Thu Jun 27 09:35:33 2013 raphael morand
** Last update Fri Jun 28 10:42:03 2013 raphael morand
*/

#include "genealfs.h"

void usage(void)
{
    printf("%s", "Usage\n\e[1;31mnative :\e[0m\n\t\t./genealfs [folder name]\n");
    printf("%s", "\e[1;31moptions :\e[0m\n\t\e[1;34mSave to file\e[0m\n");
    printf("%s", "\t\t./genealfs [folder name] -f [file name]\n");
    printf("%s", "\t\e[1;34mPipe to program\e[0m\n");
    printf("%s", "\t\t./genealf [folder name] -p [\"program (options)\"]\n");
    printf("%s", "\t\e[1;34mTermcaps Interface\e[0m\n");
    printf("%s", "\t\t./genealf [folder name] -t\n");
}

char *set_path(char *fullpath, char *name, char *d_name)
{
    if (!(fullpath = malloc(128 * sizeof(char))))
    {
        perror("malloc error");
        return (NULL);
    }
    fullpath = strcpy(fullpath, "");
    fullpath = strcat(fullpath, name);
    fullpath = strcat(fullpath, d_name);
    return (fullpath);
}

t_dlist *open_stat_read_print(t_genealfs *root, char *name,
                              t_dlist *list, t_elem *elem)
{
    t_genealfs tree;
    char *fullpath;
    int i;

    i = -1;
    while ((root->path = readdir(elem->dir)) != NULL && ++i < 19)
    {
        if (root->path->d_name[0] != '.')
        {
            if (!(fullpath = set_path(fullpath, name, root->path->d_name)))
            {
                return (NULL);
            }
            if (lstat(fullpath, &(root->type)) == -1)
            {
                return (NULL);
            }
            root->usid = getpwuid(root->type.st_uid);
            if ((root->type.st_mode & S_IFMT) == S_IFDIR)
            {
                list = handle_dir(fullpath, &tree, list);
            }
            else if ((root->type.st_mode & S_IFMT) == S_IFLNK)
            {
                elem->cnt = handle_links(fullpath, root->path->d_name, root, elem);
            }
            else if ((root->type.st_mode & S_IFMT) == S_IFREG)
            {
                elem->cnt = handle_files(fullpath, root->path->d_name, root, elem);
            }
            free(fullpath);
        }
    }
    return (list);
}

t_dlist *genealfs(char *name, t_genealfs *gene, t_dlist *list)
{
    t_elem *elem;

    elem = pushback(list, name);
    if ((elem->dir = opendir(name)) == 0)
    {
        perror("opendir error");
        return (NULL);
    }
    if ((open_stat_read_print(gene, name, list, elem)) == NULL) {
        return (NULL);
    }
    return (list);
}

int main(int ac, char **av)
{
    t_genealfs root;
    t_dlist list;

    init(&list);
    if (ac > 1 && (av[1][0] == '.' || av[1][0] == '/'))
    {
        printf(".*/ or /*/ is not a valid path or directory\n");
        return (-1);
    }
    else if (ac == 2)
        no_options(av[1], &root, &list);
    else if (ac == 3)
    {
        set_input_mode();
        option_4(av[1], av[2], &root, &list);
    }
    else if (ac == 4)
    {
        option_2o3(av, &root, &list);
    }
    else
        usage();
    return (0);
}
