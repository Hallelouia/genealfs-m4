/*
** view_list.c for view in /home/morand_c//my_select
** 
** Made by raphael morand
** Login   <morand_c@epitech.net>
** 
** Started on  Wed Jan 16 10:26:37 2013 raphael morand
** Last update Thu Jun 27 09:33:02 2013 raphael morand
*/

#include "genealfs.h"

void print(t_elem *elem, int i)
{
    printf("\e[1;34m%s\e[0m\n", elem->data);
    if (elem->mot == -1)
        printf("\e[1;36mmother\e[0m -> Unknown\n");
    if (elem->fat == -1)
        printf("\e[1;36mfather\e[0m -> Unknown\n");
    if (elem->cnt > 0)
    {
        while (++i < elem->cnt)
        {
            if (i == elem->mot)
                printf("\e[1;36mmother\e[0m -> \e[1;35m%s\e[0m\nBirth : %s\n%d\n",
                       elem->name[i], elem->dob[i], elem->pc[i]);
            else if (i == elem->fat)
                printf("\e[1;36mfather\e[0m -> \e[1;35m%s\e[0m\nBirth : %s\n%d\n",
                       elem->name[i], elem->dob[i], elem->pc[i]);
            else if (i != elem->fat || i != elem->mot)
                printf("\e[1;36mchild\e[0m -> \e[1;35m%s\e[0m\nBirth : %s\n%d\n",
                       elem->name[i], elem->dob[i], elem->pc[i]);
        }
    }
}

void view(t_dlist *l)
{
    t_elem *elem;
    int i;

    i = -1;
    l->last->next = NULL;
    elem = l->first;
    while (elem != NULL)
    {
        print(elem, i);
        elem = elem->next;
        write(1, "\n", 1);
        i = -1;
    }
    l->last->next = l->first;
}
