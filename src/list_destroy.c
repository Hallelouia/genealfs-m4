/*
** destroy_list.c for destroy in /home/morand_c//my_select
** 
** Made by raphael morand
** Login   <morand_c@epitech.net>
** 
** Started on  Wed Jan 16 10:23:18 2013 raphael morand
** Last update Thu Jun 27 11:32:20 2013 raphael morand
*/

#include "genealfs.h"

void listclear(t_dlist *l)
{
    t_elem *tmp;
    t_elem *pelem;

    pelem = l->first;
    while (pelem)
    {
        tmp = pelem;
        pelem = pelem->next;
        free(tmp);
    }
    l->first = NULL;
    l->last = NULL;
}
