/*
** start_extend.c for  in /home/morand_c//genealfs-m4/genealfs/src
** 
** Made by raphael morand
** Login   <morand_c@epitech.net>
** 
** Started on  Thu Jun 27 10:48:54 2013 raphael morand
** Last update Fri Jun 28 09:21:50 2013 raphael morand
*/

#include "genealfs.h"

void set_input_mode(void)
{
    struct termios tattr;

    if ((tcgetattr(STDIN_FILENO, &tattr)) == -1)
        exit(-1);
    tattr.c_lflag &= ~(ICANON | ECHO);
    tattr.c_cc[VMIN] = 1;
    tattr.c_cc[VTIME] = 0;
    if ((tcsetattr(STDIN_FILENO, TCSAFLUSH, &tattr)) == -1)
        exit(-1);
}

int no_options(char *av, t_genealfs *root, t_dlist *list)
{
    if ((list = genealfs(av, root, list)) != NULL)
        view(list);
    closedirs(list);
    return (0);
}

int option_2o3(char **av, t_genealfs *root,
               t_dlist *list)
{
    fct opti_tab[3];
    int opt;

    opt_tab(opti_tab);
    if ((opt = check_option(av[2])) == -1)
    {
        puts("error : wrong option");
        usage();
        return (-1);
    }
    opti_tab[opt](av[1], av[3], root, list);
    return (0);
}

int option_4(char *av1, char *opt, t_genealfs *root,
             t_dlist *list)
{
    if ((strcmp(opt, "-t")) != 0)
    {
        return (-1);
        usage();
    }
    list->pos = 1;
    list = genealfs(av1, root, list);
    if (list != NULL)
        my_select(list);
    closedirs(list);
    return (0);
}
