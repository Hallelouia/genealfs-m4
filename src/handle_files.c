/*
** handle_files.c for  in /home/morand_c//genealfs-m4/genealfs/src
** 
** Made by raphael morand
** Login   <morand_c@epitech.net>
** 
** Started on  Thu Jun 27 09:34:43 2013 raphael morand
** Last update Thu Jun 27 09:34:43 2013 raphael morand
*/

#include "genealfs.h"

int handle_files(char *fullpath, char *shortname,
                 t_genealfs *root, t_elem *elem)
{
    FILE *fd;
    size_t size;
    char *birthdate;

    if ((fd = fopen(fullpath, "r")) == NULL)
        return (-1);
    getline(&birthdate, &size, fd);
    transform_ret(birthdate);
    strcpy(elem->name[elem->cnt], shortname);
    strcpy(elem->dob[elem->cnt], birthdate);
    elem->pc[elem->cnt] = (int)root->usid->pw_uid;
    elem->cnt++;
    fclose(fd);
    return (elem->cnt);
}
