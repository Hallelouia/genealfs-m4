/*
** handle_dir.c for  in /home/morand_c//genealfs-m4/genealfs/src
** 
** Made by raphael morand
** Login   <morand_c@epitech.net>
** 
** Started on  Thu Jun 27 09:34:27 2013 raphael morand
** Last update Thu Jun 27 11:08:19 2013 raphael morand
*/

#include "genealfs.h"

t_dlist *handle_dir(char *fullpath, t_genealfs *tree, t_dlist *list)
{
    fullpath = strcat(fullpath, "/");
    return genealfs(fullpath, tree, list);
}
