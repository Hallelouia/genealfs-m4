/*
** list_create.c for  in /home/morand_c//genealfs-m4/genealfs/src
** 
** Made by raphael morand
** Login   <morand_c@epitech.net>
** 
** Started on  Thu Jun 27 09:36:30 2013 raphael morand
** Last update Thu Jun 27 09:36:30 2013 raphael morand
*/

#include "genealfs.h"

void init(t_dlist *l)
{
    l->first = NULL;
    l->last = NULL;
    l->size = 0;
}

t_elem *pushback(t_dlist *l, char *val)
{
    t_elem *nouv;

    if ((nouv = malloc(sizeof(t_elem))) == NULL)
        exit(EXIT_FAILURE);
    l->size++;
    nouv->data = strdup(val);
    nouv->next = NULL;
    nouv->prev = l->last;
    if (l->size == 1)
        nouv->point = 1;
    else
        nouv->point = 0;
    if (l->last)
        l->last->next = nouv;
    else
        l->first = nouv;
    l->last = nouv;
    nouv->cnt = 0;
    nouv->mot = -1;
    nouv->fat = -1;
    return (nouv);
}

void circularize(t_dlist *dlist)
{
    dlist->first->prev = dlist->last;
    dlist->last->next = dlist->first;
}
