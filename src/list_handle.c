/*
** my_dll_handle.c for dll_handle in /home/morand_c//SVN/myselect-2017-morand_c
** 
** Made by raphael morand
** Login   <morand_c@epitech.net>
** 
** Started on  Wed Jan 16 11:03:45 2013 raphael morand
** Last update Thu Jun 27 09:33:15 2013 raphael morand
*/

#include "genealfs.h"

void del(t_dlist *dlist, t_elem *elem)
{
    if (elem == dlist->last)
    {
        dlist->last = elem->prev;
        elem->prev->next = dlist->first;
        dlist->first->prev = elem->prev;
    }
    else if (elem == dlist->first)
    {
        dlist->first = elem->next;
        elem->next->prev = dlist->last;
        dlist->last->next = elem->next;
    }
    else
    {
        elem->next->prev = elem->prev;
        elem->prev->next = elem->next;
    }
    free(elem);
}

void dlist_rm_elemn(t_dlist *dlist)
{
    int n;
    t_elem *elem;

    elem = dlist->first;
    n = 1;
    if (dlist->size <= 1)
    {
        /* if ((tgetent(NULL, "xterm")) == -1) */
        /* x	exit(EXIT_FAILURE); */
        free(elem);
        /* my_putstr(tgetstr("cl", NULL)); */
        exit(LIST_EMPTY);
    }
    while (elem != NULL && n <= (dlist->pos))
    {
        if (n == (dlist->pos))
            del(dlist, elem);
        else
            elem = elem->next;
        n++;
    }
}
