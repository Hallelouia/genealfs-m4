/*
** list_view_tc.c for  in /home/morand_c//genealfs-m4/genealfs
** 
** Made by raphael morand
** Login   <morand_c@epitech.net>
** 
** Started on  Thu Jun 27 13:44:14 2013 raphael morand
** Last update Thu Jun 27 13:55:23 2013 raphael morand
*/

#include "genealfs.h"

void printsp(t_elem *elem, int i, t_tsave *s)
{
    printf("\e[1;34m%s%s%s%s\e[0m\n", s->us, s->mr, elem->data, s->me);
    if (elem->mot == -1)
        printf("\e[1;36mmother\e[0m -> Unknown\n");
    if (elem->fat == -1)
        printf("\e[1;36mfather\e[0m -> Unknown\n");
    if (elem->cnt > 0)
    {
        while (++i < elem->cnt)
        {
            if (i == elem->mot)
                printf("\e[1;36mmother\e[0m -> \e[1;35m%s\e[0m\nBirth : %s\n%d\n",
                       elem->name[i], elem->dob[i], elem->pc[i]);
            else if (i == elem->fat)
                printf("\e[1;36mfather\e[0m -> \e[1;35m%s\e[0m\nBirth : %s\n%d\n",
                       elem->name[i], elem->dob[i], elem->pc[i]);
            else if (i != elem->fat || i != elem->mot)
                printf("\e[1;36mchild\e[0m -> \e[1;35m%s\e[0m\nBirth : %s\n%d\n",
                       elem->name[i], elem->dob[i], elem->pc[i]);
        }
    }
}

void prints(t_elem *elem, int i, t_tsave *s)
{
    printf("\e[1;34m%s%s%s\e[0m\n", s->mr, elem->data, s->me);
    if (elem->mot == -1)
        printf("\e[1;36mmother\e[0m -> Unknown\n");
    if (elem->fat == -1)
        printf("\e[1;36mfather\e[0m -> Unknown\n");
    if (elem->cnt > 0)
    {
        while (++i < elem->cnt)
        {
            if (i == elem->mot)
                printf("\e[1;36mmother\e[0m -> \e[1;35m%s\e[0m\nBirth : %s\n%d\n",
                       elem->name[i], elem->dob[i], elem->pc[i]);
            else if (i == elem->fat)
                printf("\e[1;36mfather\e[0m -> \e[1;35m%s\e[0m\nBirth : %s\n%d\n",
                       elem->name[i], elem->dob[i], elem->pc[i]);
            else if (i != elem->fat || i != elem->mot)
                printf("\e[1;36mchild\e[0m -> \e[1;35m%s\e[0m\nBirth : %s\n%d\n",
                       elem->name[i], elem->dob[i], elem->pc[i]);
        }
    }
}
