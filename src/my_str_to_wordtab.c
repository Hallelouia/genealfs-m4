/*
** my_str_to_wordtab.c for my_str_to_wordtab in /home/hallelouia/TP_minishell
** 
** Made by raphael morand
** Login   <morand_c@epitech.eu>
** 
** Started on  Wed Dec 19 11:04:59 2012 raphael morand
** Last update Thu Jun 27 11:31:24 2013 raphael morand
*/

#include "genealfs.h"

int count_word(char *str, char c)
{
    int i;
    int j;

    i = 0;
    j = 0;
    while (str[i] != 0)
    {
        if (str[i] == c)
            j++;
        i++;
    }
    return (i + 1);
}

int my_strnlen(char *str, char c)
{
    int i;

    i = 0;
    while (str[i] && str[i] != c)
        i++;
    return (i);
}

void check_malloc(void *ptr)
{
    if (ptr == NULL)
    {
        write(2, "Malloc failed.", 14);
        exit(1);
    }
}

void nullify_end(char **mytab)
{
    int i;

    i = -1;
    while (mytab[++i][0] != 0)
        ;
    mytab[i] = NULL;
}

char **my_str_to_wordtab(char *str, char c)
{
    char **result;
    int i;
    int j;
    int count;

    i = -1;
    count = 0;
    if (str == NULL)
        return (NULL);
    result = malloc((count_word(str, c) + 1) * sizeof(*result));
    check_malloc(result);
    while (++i < count_word(str, c))
    {
        while (str[count] == c)
            count++;
        result[i] = malloc(my_strnlen(&str[count], c) + 1);
        check_malloc(result[i]);
        j = -1;
        while (str[count] != c && str[count] != 0 && str[count] != '\n')
            result[i][++j] = str[count++];
        result[i][j + 1] = '\0';
    }
    nullify_end(result);
    return (result);
}
