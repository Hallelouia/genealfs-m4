/*
** handle_links.c for  in /home/morand_c//genealfs-m4/genealfs/src
** 
** Made by raphael morand
** Login   <morand_c@epitech.net>
** 
** Started on  Thu Jun 27 09:34:54 2013 raphael morand
** Last update Thu Jun 27 09:34:54 2013 raphael morand
*/

#include "genealfs.h"

void transform_ret(char *date)
{
    int i;

    i = -1;
    while (date[++i] != '\n')
        ;
    date[i] = 0;
}

void set_parenthood(t_elem *e, char *name)
{
    if (strcmp(name, "mother") == 0)
        e->mot = e->cnt;
    else if (strcmp(name, "father") == 0)
        e->fat = e->cnt;
}

int handle_links(char *fullpath, char *shortname,
                 t_genealfs *root, t_elem *elem)
{
    FILE *fd;
    size_t size;
    char *birthdate;

    if (!(root->lkname = malloc(root->type.st_mode)))
        return (-1);
    if (!(readlink(fullpath, root->lkname, root->type.st_size + 1)))
        return (-1);
    root->lkname[root->type.st_size] = 0;
    if ((fd = fopen(fullpath, "r")) == NULL)
        return (-1);
    getline(&birthdate, &size, fd);
    transform_ret(birthdate);
    set_parenthood(elem, shortname);
    strcpy(elem->name[elem->cnt], root->lkname);
    strcpy(elem->dob[elem->cnt], birthdate);
    elem->pc[elem->cnt] = (int)root->usid->pw_uid;
    elem->cnt++;
    fclose(fd);
    return (elem->cnt);
}
