##
## Makefile for genealfs make in /home/hallelouia/genealfs
## 
## Made by Raphael Morand
## Login   <morand_c@epitech.net>
## 
## Started on  Thu Jun 20 20:37:13 2013 Raphael Morand
## Last update Fri Jun 28 10:41:39 2013 raphael morand
##

NAME	=	genealfs

SRC	=	src/start.c \
		src/start_extend.c \
		src/handle_dir.c \
		src/close.c \
		src/handle_links.c \
		src/handle_files.c \
		src/handle_option.c \
		src/handle_termcaps.c \
		src/handle_termcaps2.c \
		src/list_view_tc.c \
		src/list_create.c \
		src/list_handle.c \
		src/list_view.c \
		src/my_str_to_wordtab.c \
		src/list_destroy.c

OBJ	=	$(SRC:.c=.o)

CC	=	gcc

CFLAGS	=	-W -Wall -Wextra -Werror -I./includes/

$(NAME)	:	$(OBJ)
		$(CC) -o $(NAME) $(OBJ) -lcurses

all	:	$(NAME)

clean	:
		rm -f $(OBJ)

fclean	:	clean
		rm -f $(NAME)

re	:	fclean all

.phony	:	all clean fclean
