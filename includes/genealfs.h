/*
** genealfs.h for $ in /home/hallelouia/genealfs
** 
** Made by Raphael Morand
** Login   <morand_c@epitech.net>
** 
** Started on  Fri Jun 21 15:20:14 2013 Raphael Morand
** Last update Fri Jun 28 09:19:57 2013 raphael morand
*/

#ifndef GENEALFS_H_
#define GENEALFS_H_

#define LIST_EMPTY (0)
#define TOUCH_UP (0x00415B1B)
#define TOUCH_DOWN (0x00425B1B)
#define TOUCH_RIGHT (0x00435B1B)
#define TOUCH_LEFT (0x00445B1B)
#define TOUCH_SPACE (0x00000020)
#define TOUCH_ENTER (0x0000000A)
#define TOUCH_ESC (0x0000001B)
#define TOUCH_DEL (0x7E335B1B)
#define TOUCH_BCKSPACE (0x0000007F)

#include <dirent.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <curses.h>
#include <term.h>
#include <termcap.h>
#include <termios.h>
#include <signal.h>
#include <sys/ioctl.h>

typedef struct s_tsave
{
    char *cl;
    char *cm;
    char *us;
    char *mr;
    char *me;

} t_tsave;

typedef struct s_genealfs
{
    struct passwd *usid;
    struct dirent *path;
    struct stat type;
    int size;
    ssize_t r;
    char *lkname;
} t_genealfs;

typedef struct s_elem
{
    int cnt;
    int select;
    int point;
    char *data;
    char dob[20][30];
    int pc[20];
    int mot;
    int fat;
    char name[20][256];
    DIR *dir;
    struct s_elem *prev;
    struct s_elem *next;
} t_elem;

typedef struct s_dlist
{
    int pos;
    int size;
    t_elem *first;
    t_elem *last;
} t_dlist;

typedef int (*fct)(char *, char *, t_genealfs *, t_dlist *);

void closedirs(t_dlist *);
void view(t_dlist *);
void del(t_dlist *dlist, t_elem *elem);
void dlist_rm_elemn(t_dlist *dlist);
void init(t_dlist *l);
void circularize(t_dlist *dlist);
t_elem *pushback(t_dlist *l, char *val);
t_dlist *xnext(t_dlist *);
t_dlist *xprev(t_dlist *);
t_dlist *xselect(t_dlist *);
void my_select(t_dlist *);
void set_input_mode();
void circularize(t_dlist *);
void usage();
fct *opt_tab(fct *);
int check_option(char *opt);
t_dlist *open_stat_read_print(t_genealfs *, char *,
                              t_dlist *, t_elem *);
t_dlist *genealfs(char *, t_genealfs *, t_dlist *);
int handle_links(char *, char *, t_genealfs *, t_elem *);
int handle_files(char *, char *, t_genealfs *, t_elem *);
t_dlist *handle_dir(char *, t_genealfs *, t_dlist *);
void transform_ret(char *);
char **my_str_to_wordtab(char *str, char c);
int my_strnlen(char *str, char c);
int count_word(char *str, char c);
void end_tab(char **);
int no_options(char *, t_genealfs *, t_dlist *);
int option_2o3(char **, t_genealfs *, t_dlist *);
int option_4(char *, char *, t_genealfs *, t_dlist *);
void printsp(t_elem *, int, t_tsave *);
void prints(t_elem *, int, t_tsave *);

#endif
